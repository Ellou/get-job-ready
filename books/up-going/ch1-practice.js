"use strict"
/**
 * Write a program to calculate the total price of your phone purchase. You will keep purchasing phones (hint: loop!) until you run out of money in your bank account. You'll also buy accessories for each phone as long as your purchase amount is below your mental spending threshold.
 * After you've calculated your purchase amount, add in the tax, then print out the calculated purchase amount, properly formatted.
 * Finally, check the amount against your bank account balance to see if you can afford it or not.
 * You should set up some constants for the "tax rate," "phone price," "accessory price," and "spending threshold," as well as a variable for your "bank account balance.""
 * You should define functions for calculating the tax and for formatting the price with a "$" and rounding to two decimal places.
 * **Bonus Challenge:** Try to incorporate input into this program, perhaps with the `prompt(..)` covered in "Input" earlier. You may prompt the user for their bank account balance, for example. Have fun and be creative!
 */

/**
 * solution to practice problem
 */

// setup readline so it can be used to prompt the user for their bank balance
const readline = require('readline')
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const TAX_RATE = .06;
const PHONE_PRICE = 85.00;
const ACCESSORY_PRICE = 34.00;
var account_balance = 2000;
var spending_threshold = 200;
var tax = 0;
var total_spent = 0;
var phones_purchased = 0;
var accessories_purchased = 0;
var calculate_tax = function(item_price) { return item_price * TAX_RATE; }
var format_price = function(item_price) { return '$'+ Number(Math.round(item_price+'e'+2)+'e-'+2).toFixed(2); }

rl.question('Please Enter your account balance: ', (answer) => {
  account_balance = answer;
  console.log();console.log();
  rl.close();
});

while (account_balance > PHONE_PRICE + calculate_tax(PHONE_PRICE)) {
    tax = calculate_tax(PHONE_PRICE);
    if (account_balance - PHONE_PRICE - tax > 0) {
        account_balance -= PHONE_PRICE - tax;
        total_spent += PHONE_PRICE + tax;
        phones_purchased++;
        console.log('Purchasing one phone for: '+ format_price(PHONE_PRICE+tax));
    }
    tax = calculate_tax(ACCESSORY_PRICE);
    if (account_balance - ACCESSORY_PRICE - tax > spending_threshold) {
        account_balance -= ACCESSORY_PRICE - tax;
        total_spent += ACCESSORY_PRICE + tax;
        accessories_purchased++;
        console.log('Purchasing one accessory for: '+ format_price(ACCESSORY_PRICE+tax));
    }
    console.log();
}
console.log("Phones purchased: "+phones_purchased+" Accesories purchased: "+accessories_purchased);
console.log("Total spent: "+format_price(total_spent)+" Remaining account balance: "+format_price(account_balance));
